﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using EV.Public.ReportGraph.Data;
using Microsoft.Office.Interop.Word;


namespace EV.Public.ReportGraph.Utils
{
    public static class Helpers
    {
        private enum Formats
        {
            General = 0,
            Number = 1,
            Decimal = 2,
            Currency = 164,
            Accounting = 44,
            DateShort = 14,
            DateLong = 165,
            Time = 166,
            Percentage = 10,
            Fraction = 12,
            Scientific = 11,
            Text = 49
        }

        public static  string GetCellValue(SpreadsheetDocument document, CellType cell)
        {
            var stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
                return "";
            var value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                return stringTablePart.SharedStringTable.ChildElements[int.Parse(value)].InnerText;

            if (cell.StyleIndex == null)
                return value;

            var styleIndex = (int)cell.StyleIndex.Value;
            var cellFormat = (CellFormat)document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex);
            var formatId = cellFormat.NumberFormatId.Value;

            if (formatId == (uint)Formats.DateShort || formatId == (uint)Formats.DateLong)
            {
                if (double.TryParse(cell.InnerText, out var oaDate))
                {
                    value = DateTime.FromOADate(oaDate).ToShortDateString();
                }
            }
            else
            {
                value = cell.InnerText;
            }

            return value;
            /*
            // date value?
            if (cell.StyleIndex?.Value != 2) return value;
            try
            {
                return DateTime.FromOADate(Convert.ToDouble(value)).ToString("dd-MM-yyyy");
            }
            catch 
            {
                return value;
            }
            */
        }

        public static bool DocToPDF(string sourcefile, string destfile)
        {
            try
            {
                var appWord = new Application();
                appWord.Documents.Open(sourcefile).ExportAsFixedFormat(destfile, WdExportFormat.wdExportFormatPDF);
                appWord.Quit(false);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static Stream ConvertToBase64(this Stream stream)
        {
            stream.Position = 0;
            stream.Seek(0, SeekOrigin.Begin);

            byte[] bytes;
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                bytes = memoryStream.ToArray();
            }
            return new MemoryStream(Encoding.UTF8.GetBytes(Convert.ToBase64String(bytes)));
        }

        public static void SourcePeak(JSONRepObject curdro, Logger logger)
        {
            if (curdro.dataStream.Length != 0 && !string.IsNullOrEmpty(logger.DebugFolder))
            {
                using (Stream savefile = File.OpenWrite($"{logger.DebugFolder}\\z-worddoc-JSON.docx"))
                {
                    curdro.dataStream.Position = 0;
                    curdro.dataStream.CopyTo(savefile);
                }
                curdro.dataStream.Position = 0;
            }


            if (!curdro.reportgraphs.Any()) return;
           
            foreach (var excelelemts in curdro.reportgraphs)
            {
                if (excelelemts.dataStream == null) return;
                try
                {
                    if (string.IsNullOrEmpty(logger.DebugFolder)) return;
                    var filename = $"{logger.DebugFolder}\\z-Excel-{excelelemts.index}-JSON.xlsx";
                    using (Stream savefile = File.OpenWrite(filename))
                    {
                        excelelemts.dataStream.Position = 0;
                        excelelemts.dataStream.CopyTo(savefile);
                    }
                    excelelemts.dataStream.Position = 0;
                }
                catch (Exception ex)
                {
                    throw new Exception($"SourcePeak fejlede med følgende: {ex.Message}");
                }
            }
        }
    }
}

