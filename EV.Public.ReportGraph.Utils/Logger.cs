﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.Public.ReportGraph.Utils
{
    public class Logger
    {
        public long Totalms = 0;
        private readonly StringBuilder _logsb = new StringBuilder();
        public string DebugFolder = string.Empty;
        public string ZipFileName = string.Empty;
        private readonly bool _showDebugMsg;

        public Logger(bool showdebugmessages)
        {
            _showDebugMsg = showdebugmessages;
        }

        public void Add(string text, long duratation)
        {
            var debustr = duratation > 0 ? $"{DateTime.Now} : {text} [Tid: {duratation}ms] {Environment.NewLine}" : $"{DateTime.Now} : {text} {Environment.NewLine}";
            _logsb.Append(debustr);

            if (!_showDebugMsg) return;
            Console.Write(debustr);
        }


        public void Add(string text)
        {
            var debustr = $"{DateTime.Now} : {text} {Environment.NewLine}";
            _logsb.Append(debustr);
            if (!_showDebugMsg) return;
            Console.Write(debustr);
        }

        public void AddError(string text)
        {
            text = "Fejl: " + text;
            Add(text);
        }

        public void AdderrorException(string text)
        {
            text = "Fejl: " + text;
            Add(text);
            throw new Exception(text);
        }

        public Stopwatch Start()
        {
            var temp = new Stopwatch();
            temp.Start();
            return temp;
        }

        public Stopwatch Start(string starttext)
        {
            if (!string.IsNullOrEmpty(starttext))
                Add("Start: " + starttext);

            var temp = new Stopwatch();
            temp.Start();
            return temp;
        }

        public void Stop(string endtext)
        {
            Add("Stop: " + endtext);
        }

        public void Stop(string endtext, Stopwatch curtimer)
        {
            curtimer.Stop();
            Add("Stop: " + endtext, curtimer.ElapsedMilliseconds);
            //if (maintimer != null && maintimer.Value == true)
            //    Totalms += curtimer.ElapsedMilliseconds;
        }


        public void SaveToFile(string filename, long maintimer)
        {
            var ts = TimeSpan.FromMilliseconds(maintimer);
            Add($"EOR [{ts.Hours:D2}t:{ts.Minutes:D2}m:{ts.Seconds:D2}s:{ts.Milliseconds:D3}ms]");
            File.WriteAllText(filename, _logsb.ToString());
        }

    }

}
