﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;

namespace EV.Public.ReportGraph.Data
{
    public static class DocComment
    {
        public static void Set(Stream docstream, string newcomment)
        {
            docstream.Position = 0;
            docstream.Seek(0, SeekOrigin.Begin);
            using (var doc = WordprocessingDocument.Open(docstream, true))
            {
                doc.PackageProperties.Version = newcomment;
                doc.PackageProperties.Description = newcomment;
                doc.Save();
            }
        }
    }
}
