﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.Public.ReportGraph.Data
{
    public class JSONRepObject
    {
        public string worddoc { get; set; }
        public List<Reportgraph> reportgraphs { get; set; }
        public Stream dataStream;
    }

    public class LineOptions
    {
        public bool TransposeDiagram { get; set; } = false;

        public string CaptionDiagram { get; set; } = string.Empty;
    }

    public class Reportgraph
    {
        public string index { get; set; }
        public string excelfil { get; set; }
        public string options { get; set; }
        public LineOptions lineOptions;

        public Stream dataStream;
        public int nrused;
    }
}
