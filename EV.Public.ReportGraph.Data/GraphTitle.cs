﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using OpenXmlPowerTools;
using Cell = DocumentFormat.OpenXml.Spreadsheet.Cell;
using Row = DocumentFormat.OpenXml.Spreadsheet.Row;
// ReSharper disable UnusedMember.Local

namespace EV.Public.ReportGraph.Data
{
    public static class GraphTitle
    {

        private enum Formats
        {
            General = 0,
            Number = 1,
            Decimal = 2,
            Currency = 164,
            Accounting = 44,
            DateShort = 14,
            DateLong = 165,
            Time = 166,
            Percentage = 10,
            Fraction = 12,
            Scientific = 11,
            Text = 49
        }
        private static string GetCellValue(SpreadsheetDocument document, CellType cell)
        {
            var stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
                return "";
            var value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                return stringTablePart.SharedStringTable.ChildElements[int.Parse(value)].InnerText;

            if (cell.StyleIndex == null)
                return value;

            var styleIndex = (int)cell.StyleIndex.Value;
            var cellFormat = (CellFormat)document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex);
            var formatId = cellFormat.NumberFormatId.Value;

            if (formatId == (uint)Formats.DateShort || formatId == (uint)Formats.DateLong)
            {
                if (double.TryParse(cell.InnerText, out var oaDate))
                {
                    value = DateTime.FromOADate(oaDate).ToShortDateString();
                }
            }
            else
                value = cell.InnerText;

            return value;
        }

        public static string GetExcelTitle(Stream excelstream)
        {
            var retstr = string.Empty;
            excelstream.Position = 0;
            excelstream.Seek(0, SeekOrigin.Begin);

            using (var spreadSheetDocument = SpreadsheetDocument.Open(excelstream, false))
            {
                var sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                var relationshipId = sheets.First().Id.Value;
                var worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                var workSheet = worksheetPart.Worksheet;
                var sheetData = workSheet.GetFirstChild<SheetData>();
                var rows = sheetData.Descendants<Row>();

                var enumRows = rows as Row[] ?? rows.ToArray();
                if (enumRows.Any())
                {
                    var firstrow = enumRows.ElementAt(0);
                    if (string.IsNullOrEmpty(firstrow.InnerText)) return string.Empty;

                    // First cell
                    var firstcell = firstrow.Descendants<Cell>().FirstOrDefault();
                    if (firstcell != null && firstcell.Any())
                        return GetCellValue(spreadSheetDocument, firstcell);
                }
            }
            return retstr;
        }

        public static void UpdateTitle(ChartPart chartpart, string newtitle)
        {
            var cpXDoc = chartpart.GetXDocument();
            var root = cpXDoc.Root;

            var titleelement = root?.Descendants(C.title).FirstOrDefault();
            var txElement = titleelement?.Descendants(C.tx).FirstOrDefault();
            if (txElement == null)
            {
                var richelement = new XElement(C.tx,
                    new XElement(C.rich, new XElement(A.p, new XElement(A.r, new XElement(A.t, newtitle)))));

                var insertBeforeElement = titleelement?.Descendants(C.overlay).FirstOrDefault();
                if (insertBeforeElement != null)
                    insertBeforeElement.AddBeforeSelf(richelement);
                else
                    titleelement?.Add(richelement);

            }
            else
            {
                //var txParagraph = txElement.Descendants(C.rich).FirstOrDefault()
                //    ?.Descendants(A.p).FirstOrDefault();

                var txParagraphList = txElement.Descendants(C.rich).Descendants(A.p).ToList();
                foreach (var txParagraph in txParagraphList)
                {
                    var oldRuns = txParagraph?.Descendants(A.r).ToList();
                    var firstrun = true;
                    if (oldRuns != null)
                    {
                        foreach (var runs in oldRuns)
                        {
                            var elements = runs?.Elements().ToList();
                            runs?.Remove();

                            var newRun = new XElement(A.r);
                            if (elements != null)
                            {
                                foreach (var element in elements)
                                {
                                    if (element.Name != A.t)
                                        newRun.Add(element);
                                }
                            }

                            // only one title line! (No support for 2 titles eg. WireFrame 3D).
                            if (firstrun)
                            {
                                newRun.Add(new XElement(A.t, newtitle));
                                firstrun = false;
                            }
                            else
                                newRun.Add(new XElement(A.t, string.Empty));

                            // Any a:endParaRPr ? 
                            var endPapr = txParagraph.Descendants(A.endParaRPr).FirstOrDefault();
                            if (endPapr == null)
                                txParagraph.Add(newRun);
                            else
                                endPapr.AddBeforeSelf(newRun);
                        }
                    }
                    else
                        // ReSharper disable once PossibleNullReferenceException
                        txParagraph.Add(new XElement(A.r, new XElement(A.t, newtitle)));
                }
            }

            // Is there any properties?
            var bodypr = titleelement?.Descendants(C.tx).Descendants(C.rich).Descendants(A.bodyPr).FirstOrDefault();

            if (bodypr == null)
            {
                var txtProps = titleelement?.Descendants(C.txPr).FirstOrDefault();
                var txElements = txtProps?.Elements();
                if (txElements != null)
                {
                    var xElements = txElements.ToList();
                    foreach (var txElem in xElements)
                    {
                        if (!txElem.HasElements) continue;

                        if (txElem.Element(A.endParaRPr) != null)
                            txElem.Elements(A.endParaRPr).Remove();
                    }

                    var richElement = titleelement.Descendants(C.tx).Descendants(C.rich).FirstOrDefault();
                    richElement?.AddFirst(xElements);
                }

            }

           chartpart.PutXDocument(cpXDoc);

            /* Remove all and build up 
            var txElement = titleelement?.Descendants(C.tx).FirstOrDefault();
            // Fjern evt. gl. tx element.
            txElement?.Remove();

            // Copy Text elements.
            var txtProps = titleelement?.Descendants(C.txPr).FirstOrDefault();

            XElement richelement = null;

            var txElements = txtProps?.Elements();
            if (txElements != null)
            {
                var xElements = txElements.ToList();
                foreach (var txElem in xElements)
                {
                    if (!txElem.HasElements) continue;

                    if (txElem.Element(A.endParaRPr) != null)
                        txElem.Elements(A.endParaRPr).Remove();
                }

                if (xElements.Any())
                    richelement = new XElement(C.tx, new XElement(C.rich, xElements));
            }

            if (richelement == null)
                richelement = new XElement(C.tx, new XElement(C.rich));
            
            var insertBeforeElement = titleelement?.Descendants(C.overlay).FirstOrDefault();
            if (insertBeforeElement != null)
                insertBeforeElement.AddBeforeSelf(richelement);
            else
                titleelement?.Add(richelement);

            var titlepara = richelement.Descendants(A.p).FirstOrDefault();

            var newText = new XElement(A.r);
            newText.Add(new XElement(A.t, newtitle));
            titlepara?.Add(newText);            
            chartpart.PutXDocument(cpXDoc);
            */

        }
    }
}
