﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;

namespace EV.Public.ReportGraph.Data
{

    public static class SupportedGraphs
    {
        public static bool IsValid(ChartPart chartPart, string allowedtypes)
        {
            if (chartPart == null)
                return false;

            var cpXDoc = chartPart.GetXDocument();
            var root = cpXDoc.Root;
            var firstSeries = root?.Descendants(C.ser).FirstOrDefault();
            var chartType = firstSeries?.Parent?.Name;

            // Pie (Cirkeldiagram)
            if (chartType == C.doughnutChart || chartType == C.pie3DChart || chartType == C.pieChart)
                return allowedtypes.ToLower().Contains("pie");

            // Bar (STolper / SØjler ) også liggende..
            if (chartType == C.barChart || chartType == C.bar3DChart)
                return allowedtypes.ToLower().Contains("bar") || allowedtypes.ToLower().Contains("column");

            // Line (linjegraf).
            if (chartType == C.lineChart || chartType == C.line3DChart)
                return allowedtypes.ToLower().Contains("line");

            // Område diagram
            if (chartType == C.areaChart || chartType == C.area3DChart)
                return allowedtypes.ToLower().Contains("area");

            // Grundflader
            if (chartType == C.surfaceChart || chartType == C.surface3DChart)
                return allowedtypes.ToLower().Contains("surface");

            // Radar 
            if (chartType == C.radarChart)
                return allowedtypes.ToLower().Contains("radar");

            /* chartType == C.ofPieChart || not supported */
            return false;
        }

        public static string IsTypeOf(ChartPart chartPart)
        {
            var cpXDoc = chartPart.GetXDocument();
            var root = cpXDoc.Root;
            var firstSeries = root?.Descendants(C.ser).FirstOrDefault();
            return firstSeries?.Parent?.Name.LocalName;
        }

        public static ChartDataType FindCDType(ChartPart chartpart)
        {
            var cpXDoc = chartpart.GetXDocument();
            var root = cpXDoc.Root;
            var firstSeries = root?.Descendants(C.ser).FirstOrDefault();
            var chartType = firstSeries?.Parent?.Name;
            if (chartType == C.area3DChart || chartType == C.areaChart)
                return ChartDataType.DateTime;

            return ChartDataType.String;
        }
            
    }
}
