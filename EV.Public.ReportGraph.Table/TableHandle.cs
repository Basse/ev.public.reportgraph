﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using ClosedXML;
using ClosedXML.Excel;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using EV.Public.ReportGraph.Data;
using EV.Public.ReportGraph.Utils;
using TableProperties = DocumentFormat.OpenXml.Wordprocessing.TableProperties;
using UnderlineValues = DocumentFormat.OpenXml.Wordprocessing.UnderlineValues;


namespace EV.Public.ReportGraph.Table
{
    public class TableHandle
    {

        private XLWorkbook _currentXLWorkBook { get; set; }




        private string FromExcelColor(XLColor xlcol)
        {
            var retval = "auto";
            var resColor = xlcol;

            if (xlcol.ColorType == XLColorType.Theme)
            {
                retval = TableHelpers.Theme2Rgb(xlcol.ThemeColor);
                if (!string.IsNullOrEmpty(retval)) return retval;


                var rColor = _currentXLWorkBook.Theme.ResolveThemeColor(xlcol.ThemeColor);
                var resstr = ColorTranslator.ToHtml(rColor.Color);
                if (resstr.StartsWith("#"))
                    return resstr.Substring(1);

                return resstr;
            }

            if (xlcol.ColorType == XLColorType.Indexed &&xlcol.Color.Name.Equals("Transparent", StringComparison.CurrentCultureIgnoreCase))
                return "auto";

            if (!resColor.HasValue) return retval;

            if (resColor.Color.R < 10)
                retval = "0" + resColor.Color.R.ToString("X");
            else
                retval = resColor.Color.R.ToString("X");

            if (resColor.Color.G < 10)
                retval += "0" + resColor.Color.G.ToString("X");
            else
                retval += resColor.Color.G.ToString("X");

            if (resColor.Color.B < 10)
                retval += "0" + resColor.Color.B.ToString("X");
            else
                retval += resColor.Color.B.ToString("X");

            return retval;
        }


        public void ReaplaceTables(JSONRepObject repobj, Logger logger, bool reportgraphparametersHaltOnErrors, string reportgraphparametersAllowedGraphs)
        {
            var reTimer = logger.Start("Starter erstatning af tables");
            using (var doc = WordprocessingDocument.Open(repobj.dataStream, true))
            {
                foreach (var excelelements in repobj.reportgraphs)
                {
                    if (excelelements.nrused != 0) continue;

                    var tableProperties = doc.MainDocumentPart.Document.Descendants<TableProperties>().FirstOrDefault(tp =>
                            tp.TableCaption != null && string.Equals(tp.TableCaption.Val.ToString(), excelelements.index, StringComparison.InvariantCultureIgnoreCase));
                    if (tableProperties != null)
                    {
                        excelelements.nrused = 1;
                        var table = (DocumentFormat.OpenXml.Wordprocessing.Table)tableProperties.Parent;

                        table.ClearAllAttributes();
                        table.RemoveAllChildren();


                        var tbTableProp = new TableProperties
                        {
                            //TableBorders = new TableBorders()
                            //{

                            //},
                            TableCaption = new TableCaption()
                            {
                                Val = excelelements.index
                            }
                        };

                        table.AppendChild(tbTableProp);

                        // test
                        //TableHelpers.GemBoxTest(excelelements.dataStream);
                        //excelelements.dataStream.Position = 0;
                        //excelelements.dataStream.Seek(0, SeekOrigin.Begin);

                        using (var clXmldoc = new XLWorkbook(excelelements.dataStream))
                        {
                            _currentXLWorkBook = clXmldoc;

                            var cursheet = clXmldoc.Worksheets.FirstOrDefault();
                            if (cursheet != null)
                            {
                                var rows = cursheet.Rows(1, cursheet.LastRowUsed().RowNumber());
                                foreach (var _row in rows) //cursheet.Rows()
                                {
                                    if (!_row.IsHidden)
                                    {
                                        var tr = new DocumentFormat.OpenXml.Wordprocessing.TableRow();

                                        // Height.
                                        if (Math.Abs(_row.Height) > 0)
                                        {
                                            var tHeight = _row.Height * 20;
                                            tr.AppendChild(
                                                new TableRowProperties(new TableRowHeight()
                                                {
                                                    Val = (UInt32Value) tHeight
                                                }));
                                        }


                                        bool anyCells = false;

                                        foreach (var _cells in _row.Cells())
                                        {
                                            var textval = _cells.Value.ToString(); // GetFormattedString();

                                            if (_cells.DataType == XLDataType.Number && _cells.Style.NumberFormat.NumberFormatId == 2 || _cells.Style.NumberFormat.NumberFormatId == 4 )
                                            {
                                                var dbl = Convert.ToDecimal(_cells.Value);
                                                if (_cells.Style.NumberFormat.NumberFormatId == 4 )
                                                    textval = dbl.ToString("N", CultureInfo.GetCultureInfo("da-DK"));
                                                else
                                                    textval = dbl.ToString("#0.00", CultureInfo.GetCultureInfo("da-DK"));

                                            }


                                            var cellstyle = _cells.Style;

                                            var tablecell = new DocumentFormat.OpenXml.Wordprocessing.TableCell();
                                            var tablecellproperties = new DocumentFormat.OpenXml.Wordprocessing.TableCellProperties();

                                            //var tt = _cells.WorksheetColumn();

                                            //Console.WriteLine($"Cell: [{_cells.Address.ColumnLetter}] - Width: {_cells.WorksheetColumn().Width} - ToTwips: {TableHelpers.CalcColWidth(_cells.WorksheetColumn().Width)}");
                                            //Console.WriteLine($"Row: [{_cells.Address.RowNumber}] - Heigth: {_cells.WorksheetRow().Height} ");



                                            // Width
                                            tablecellproperties.AppendChild(
                                                new DocumentFormat.OpenXml.Wordprocessing.TableCellWidth()
                                                {
                                                    Width = TableHelpers.CalcColWidth(_cells.WorksheetColumn().Width).ToString(CultureInfo.InvariantCulture), 
                                                    Type = TableWidthUnitValues.Dxa
                                                });
                                            
                                            

                                            // Borders
                                            var tablecellborders = new DocumentFormat.OpenXml.Wordprocessing.TableCellBorders();
                                            if (cellstyle.Border.TopBorder != XLBorderStyleValues.None)
                                            {
                                                var tb = TableHelpers.fromExcelBorderValues(cellstyle.Border.TopBorder);
                                                tablecellborders.AppendChild(
                                                    new DocumentFormat.OpenXml.Wordprocessing.TopBorder
                                                    {
                                                        Color = FromExcelColor(cellstyle.Border.TopBorderColor),
                                                        Size = tb.Size,
                                                        Val = tb.BorderValue,
                                                        Space = tb.Space
                                                    });
                                            }
                                            else
                                                tablecellborders.AppendChild(
                                                    new DocumentFormat.OpenXml.Wordprocessing.TopBorder
                                                    {
                                                        Val = BorderValues.Nil
                                                    });

                                            if (cellstyle.Border.LeftBorder != XLBorderStyleValues.None)
                                            {
                                                var lb = TableHelpers.fromExcelBorderValues(cellstyle.Border.LeftBorder);
                                                tablecellborders.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.LeftBorder
                                                {
                                                    Color = FromExcelColor(cellstyle.Border.LeftBorderColor),
                                                    Size = lb.Size,
                                                    Val = lb.BorderValue,
                                                    Space = lb.Space
                                                });
                                            }
                                            else
                                                tablecellborders.AppendChild(
                                                    new DocumentFormat.OpenXml.Wordprocessing.LeftBorder
                                                    {
                                                        Val = BorderValues.Nil
                                                    });

                                            if (cellstyle.Border.BottomBorder != XLBorderStyleValues.None)
                                            {
                                                var bb = TableHelpers.fromExcelBorderValues(cellstyle.Border.BottomBorder);
                                                tablecellborders.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.BottomBorder
                                                {
                                                    Color = FromExcelColor(cellstyle.Border.BottomBorderColor),
                                                    Size = bb.Size,
                                                    Val = bb.BorderValue,
                                                    Space = bb.Space
                                                });
                                            }
                                            else
                                                tablecellborders.AppendChild(
                                                    new DocumentFormat.OpenXml.Wordprocessing.BottomBorder
                                                    {
                                                        Val = BorderValues.Nil
                                                    });

                                            if (cellstyle.Border.RightBorder != XLBorderStyleValues.None)
                                            {
                                                var rb = TableHelpers.fromExcelBorderValues(cellstyle.Border.RightBorder);
                                                tablecellborders.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.RightBorder
                                                {
                                                    Color = FromExcelColor(cellstyle.Border.RightBorderColor),
                                                    Size = rb.Size,
                                                    Val = rb.BorderValue,
                                                    Space = rb.Space
                                                });
                                            }
                                            else
                                                tablecellborders.AppendChild(
                                                    new DocumentFormat.OpenXml.Wordprocessing.RightBorder
                                                    {
                                                        Val = BorderValues.Nil
                                                    });

                                            tablecellproperties.AppendChild(tablecellborders);


                                            // Shading..
                                            if (cellstyle.Fill.PatternType != XLFillPatternValues.None)
                                            {                                               
                                                //var test = new DocumentFormat.OpenXml.Wordprocessing.Shading
                                                //{
                                                //    Color = FromExcelColor(cellstyle.Fill.BackgroundColor),
                                                //    Fill = FromExcelColor(cellstyle.Fill.PatternColor),
                                                //    Val = TableHelpers.FromExcelFillPatternValues(cellstyle.Fill.PatternType)                    
                                                //};


                                                tablecellproperties.AppendChild( new DocumentFormat.OpenXml.Wordprocessing.Shading
                                                {
                                                    Color = FromExcelColor(cellstyle.Fill.BackgroundColor),
                                                    Fill = FromExcelColor(cellstyle.Fill.PatternColor),
                                                    Val = TableHelpers.FromExcelFillPatternValues(cellstyle.Fill.PatternType)                    
                                                });
                                            }


                                            //Wrao text?
                                            var wrapState = OnOffOnlyValues.Off;
                                            if (cellstyle.Alignment.WrapText)
                                                wrapState = OnOffOnlyValues.On;

                                            tablecellproperties.AppendChild(new NoWrap() {Val = wrapState});

                                            
                                            //verticalallignment.
                                            if (cellstyle.Alignment.Vertical == XLAlignmentVerticalValues.Bottom)
                                                tablecellproperties.AppendChild(new TableCellVerticalAlignment()
                                                {
                                                    Val = TableVerticalAlignmentValues.Bottom
                                                });
                                            else if (cellstyle.Alignment.Vertical == XLAlignmentVerticalValues.Center)
                                                tablecellproperties.AppendChild(new TableCellVerticalAlignment()
                                                {
                                                    Val = TableVerticalAlignmentValues.Center
                                                });
                                            else if (cellstyle.Alignment.Vertical == XLAlignmentVerticalValues.Top)
                                                tablecellproperties.AppendChild(new TableCellVerticalAlignment()
                                                {
                                                    Val = TableVerticalAlignmentValues.Top
                                                });
                                            

                                            if (tablecellproperties.Any())
                                                tablecell.AppendChild(tablecellproperties);

                                            var paragraph = new DocumentFormat.OpenXml.Wordprocessing.Paragraph();
                                            var pProps = new DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties();

                                            pProps.AppendChild( new SpacingBetweenLines(){ After = "0", Line = "240", LineRule = LineSpacingRuleValues.Auto });


                                            if (cellstyle.Alignment.Horizontal == XLAlignmentHorizontalValues.General &&
                                                _cells.DataType == XLDataType.Number)
                                            {
                                                pProps.AppendChild(new Justification(){ Val = JustificationValues.Right });

                                            }


                                            var pMarkProps = new DocumentFormat.OpenXml.Wordprocessing.ParagraphMarkRunProperties();
                                            var rRunProps = new DocumentFormat.OpenXml.Wordprocessing.RunProperties();


                                            
                                            pMarkProps.AppendChild(new RunFonts
                                                {
                                                    Ascii = _cells.Style.Font.FontName
                                                });
                                            rRunProps.AppendChild(new RunFonts
                                            {
                                                Ascii = _cells.Style.Font.FontName
                                            });
                                               
                                            if (_cells.Style.Font.Bold)
                                            {
                                                pMarkProps.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Bold());
                                                rRunProps.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Bold());
                                            }

                                            if (_cells.Style.Font.Italic)
                                            {
                                                pMarkProps.AppendChild( new DocumentFormat.OpenXml.Wordprocessing.Italic());
                                                rRunProps.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Italic());
                                            }
                                                
                                            if (_cells.Style.Font.Underline != XLFontUnderlineValues.None)
                                            {
                                                pMarkProps.AppendChild(
                                                    new DocumentFormat.OpenXml.Wordprocessing.Underline
                                                    {
                                                        Val = UnderlineValues.Single
                                                    });
                                                rRunProps.AppendChild(
                                                    new DocumentFormat.OpenXml.Wordprocessing.Underline
                                                    {
                                                        Val = UnderlineValues.Single
                                                    });

                                            }

                                            var celcol = FromExcelColor(_cells.Style.Font.FontColor);
                                            if (!string.IsNullOrEmpty(celcol))
                                            {
                                                pMarkProps.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Color
                                                {
                                                    Val = celcol
                                                });
                                                rRunProps.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Color
                                                {
                                                    Val = celcol
                                                });
                                            }

                                            if (_cells.Style.Font.FontSize > 0)
                                            {
                                                pMarkProps.AppendChild( new DocumentFormat.OpenXml.Wordprocessing.FontSize
                                                {
                                                    Val = (_cells.Style.Font.FontSize * 2.0).ToString(CultureInfo.InvariantCulture)
                                                });
                                                rRunProps.AppendChild( new DocumentFormat.OpenXml.Wordprocessing.FontSize
                                                {
                                                    Val = (_cells.Style.Font.FontSize * 2.0).ToString(CultureInfo.InvariantCulture)
                                                });

                                                pMarkProps.AppendChild(new FontSizeComplexScript
                                                    {
                                                        Val = (_cells.Style.Font.FontSize * 2.0).ToString(CultureInfo
                                                            .InvariantCulture)
                                                    });
                                                rRunProps.AppendChild(new FontSizeComplexScript
                                                {
                                                    Val = (_cells.Style.Font.FontSize * 2.0).ToString(CultureInfo
                                                        .InvariantCulture)
                                                });

                                            }

                                            pMarkProps.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Languages
                                            {
                                                EastAsia = "da-Dk"
                                            });
                                            rRunProps.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Languages
                                            {
                                                EastAsia = "da-Dk"
                                            });


                                            pProps.AppendChild(pMarkProps);
                                            var rRun = new DocumentFormat.OpenXml.Wordprocessing.Run();

                                            rRun.AppendChild(rRunProps);
                                            rRun.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Text(textval));

                                            paragraph.AppendChild(pProps);
                                            paragraph.AppendChild(rRun);
                                            tablecell.AppendChild(paragraph);
                                               
                                            tr.AppendChild(tablecell);
                                            anyCells = true;

                                        }

                                        if (!anyCells)
                                        {
                                            var tablecell = new DocumentFormat.OpenXml.Wordprocessing.TableCell();
                                            tablecell.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.TableCellProperties());
                                            var rRun = new DocumentFormat.OpenXml.Wordprocessing.Run();
                                            var rText = new DocumentFormat.OpenXml.Wordprocessing.Text(" ");
                                            var rProp = new DocumentFormat.OpenXml.Wordprocessing.ParagraphMarkRunProperties();
                                            rRun.AppendChild(rProp);
                                            rRun.AppendChild(rText);
                                            tablecell.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(rRun));
                                            tr.AppendChild(tablecell);
                                        }
                                        table.AppendChild(tr);
                                    }
                                }
                            }
                            //}

                            //excelelements.dataStream.Position = 0;
                            //excelelements.dataStream.Seek(0, SeekOrigin.Begin);

                            //using (var spreadSheetDocument = SpreadsheetDocument.Open(excelelements.dataStream, false))
                            //{
                            //    var sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                            //    var relationshipId = sheets.First().Id.Value;
                            //    var worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                            //    var workSheet = worksheetPart.Worksheet;
                            //    var sheetData = workSheet.GetFirstChild<SheetData>();
                            //    var rows = sheetData.Descendants<Row>();


                            //    //var tbTableProp = new TableProperties
                            //    //{
                            //    //    TableBorders = new TableBorders()
                            //    //    {

                            //    //    },
                            //    //    TableCaption = new TableCaption()
                            //    //    {
                            //    //        Val = excelelements.index
                            //    //    }
                            //    //};

                            //    //table.Append(tbTableProp);

                            //    var enumRows = rows as Row[] ?? rows.ToArray();
                            //    //foreach (var openXmlElement in enumRows.ElementAt(0))
                            //    //{
                            //    //    var cell = (Cell)openXmlElement;
                            //    //    if (!String.IsNullOrEmpty(cell.InnerText))
                            //    //        dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                            //    //}

                            //    foreach (var row in enumRows)
                            //    {
                            //        var tr = new TableRow();
                            //        if (!string.IsNullOrEmpty(row.InnerText))
                            //        {
                            //            for (var i = 0; i < row.Descendants<Cell>().Count(); i++)
                            //            {
                            //                var tc = new TableCell();
                            //                var textval = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));

                            //                tc.AppendChild(new Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(childElements: new DocumentFormat.OpenXml.Wordprocessing.Text(textval))));

                            //                tr.AppendChild(tc);

                            //            }
                            //        }
                            //        _table.Append(tr);

                            //    }
                        }

                    }

                }
            }


        }

        //private static string GetCellValue(SpreadsheetDocument document, CellType cell)
        //{
        //    var stringTablePart = document.WorkbookPart.SharedStringTablePart;
        //    if (cell.CellValue == null)
        //        return "";
        //    var value = cell.CellValue.InnerXml;

        //    if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        //        return stringTablePart.SharedStringTable.ChildElements[int.Parse(value)].InnerText;

        //    // date value?
        //    if (cell.StyleIndex?.Value != 2) return value;
        //    try
        //    {
        //        return DateTime.FromOADate(Convert.ToDouble(value)).ToString("dd-MM-yyyy");
        //    }
        //    catch
        //    {
        //        return value;
        //    }
        //}

        private static string GetCellValue(WorkbookPart wbPart, IEnumerable<Cell> theCells, string cellColumnReference)
        {
            var value = "";
            var theCell = theCells.FirstOrDefault(cell => cell.CellReference.Value.StartsWith(cellColumnReference));

            if (theCell == null) return value;

            value = theCell.InnerText;
            // If the cell represents an integer number, you are done. 
            // For dates, this code returns the serialized value that represents the date. The code handles strings and 
            // Booleans individually. For shared strings, the code looks up the corresponding value in the shared string table. For Booleans, the code converts the value into the words TRUE or FALSE.
            if (theCell.DataType == null) return value;

            switch (theCell.DataType.Value)
            {
                case CellValues.SharedString:
                    // For shared strings, look up the value in the shared strings table.
                    var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                    // If the shared string table is missing, something is wrong. Return the index that is in the cell. Otherwise, look up the correct text in the table.
                    if (stringTable != null)
                    {
                        value = stringTable.SharedStringTable.ElementAt(int.Parse(value)).InnerText;
                    }
                    break;
                case CellValues.Boolean:
                    switch (value)
                    {
                        case "0":
                            value = "FALSE";
                            break;
                        default:
                            value = "TRUE";
                            break;
                    }
                    break;
            }
            return value;
        }

        private static string GetCellValue(WorkbookPart wbPart, List<Cell> theCells, int index)
        {
            return GetCellValue(wbPart, theCells, GetExcelColumnName(index));
        }

        private static string GetExcelColumnName(int columnNumber)
        {
            var dividend = columnNumber;
            var columnName = string.Empty;
            while (dividend > 0)
            {
                var modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo) + columnName;
                dividend = (dividend - modulo) / 26;
            }
            return columnName;
        }

        //Only xlsx files
        public static DataTable GetDataTableFromExcelFile(Stream filestrem, string sheetName = "")
        {
            var dt = new DataTable();
            try
            {
                using (var document = SpreadsheetDocument.Open(filestrem, false))
                {
                    var wbPart = document.WorkbookPart;
                    var sheets = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    var sheetId = sheetName != "" ? sheets.First(q => q.Name == sheetName).Id.Value : sheets.First().Id.Value;
                    var wsPart = (WorksheetPart)wbPart.GetPartById(sheetId);
                    var sheetdata = wsPart.Worksheet.Elements<SheetData>().FirstOrDefault();
                    if (sheetdata != null)
                    {
                        var totalHeaderCount = sheetdata.Descendants<Row>().ElementAt(0).Descendants<Cell>().Count();
                        //Get the header                    
                        for (var i = 1; i <= totalHeaderCount; i++)
                        {
                            dt.Columns.Add(GetCellValue(wbPart, sheetdata.Descendants<Row>().ElementAt(0).Elements<Cell>().ToList(), i));
                        }
                        foreach (var r in sheetdata.Descendants<Row>())
                        {
                            if (r.RowIndex <= 1) continue;
                            var tempRow = dt.NewRow();

                            //Always get from the header count, because the index of the row changes where empty cell is not counted
                            for (var i = 1; i <= totalHeaderCount; i++)
                            {
                                tempRow[i - 1] = GetCellValue(wbPart, r.Elements<Cell>().ToList(), i);
                            }
                            dt.Rows.Add(tempRow);
                        }
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return dt;
        }

    }
}

