﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using Color = System.Drawing.Color;
using Font = System.Drawing.Font;
using FontFamily = System.Drawing.FontFamily;

namespace EV.Public.ReportGraph.Table
{


    public static class TableHelpers
    {
        public class WordBorderValues
        {
            public BorderValues BorderValue { get; set; }
            public UInt32Value Size { get; set; }
            public UInt32Value Space { get; set; }

        }
        public static WordBorderValues fromExcelBorderValues(XLBorderStyleValues excelvalue)
        {
           
            switch (excelvalue)
            {
                //1
                case XLBorderStyleValues.None:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.None,
                        Size = 0,
                        Space = 0
                    };
                //2
                case XLBorderStyleValues.Hair:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.Single,
                        Size = 4U,
                        Space = 0
                    };
                //3
                case XLBorderStyleValues.Dotted:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.Dotted,
                        Size = 4U,
                        Space = 0
                    };
                //4
                case XLBorderStyleValues.DashDotDot:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.DotDotDash,
                        Size = 4U,
                        Space = 0
                    };
                //5
                case XLBorderStyleValues.DashDot:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.DotDash,
                        Size = 4U,
                        Space = 0
                    };
                //6
                case XLBorderStyleValues.Dashed:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.Dashed,
                        Size = 4U,
                        Space = 0
                    };
                //7
                case XLBorderStyleValues.Thin:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.Single,
                        Size = 4U,
                        Space = 0
                    };

                //8
                case XLBorderStyleValues.MediumDashDotDot:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.DotDotDash,
                        Size = 8U,
                        Space = 0
                    };
                //9
                case XLBorderStyleValues.SlantDashDot:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.DotDash,
                        Size = 8U,
                        Space = 0
                    };

                //10
                case XLBorderStyleValues.MediumDashDot:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.DotDash,
                        Size = 8U,
                        Space = 0
                    };
                //11
                case XLBorderStyleValues.MediumDashed:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.Dashed,
                        Size = 8U,
                        Space = 0
                    };

                //12
                case XLBorderStyleValues.Medium:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.Single,
                        Size = 8U,
                        Space = 0
                    };
                //13
                case XLBorderStyleValues.Thick:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.Single,
                        Size = 12U,
                        Space = 0
                    };
                //13
                case XLBorderStyleValues.Double:
                    return new WordBorderValues
                    {
                        BorderValue = BorderValues.Double,
                        Size = 6U,
                        Space = 0
                    };

            }
           
            return new WordBorderValues
            {
                BorderValue = BorderValues.None,
                Size = 0
            };
        }

        public static string Theme2Rgb(XLThemeColor xlthcol)
        {
            switch (xlthcol)
            {
                case XLThemeColor.Accent1:
                    return "4472C4";
                case XLThemeColor.Accent2:
                    return "ED7D31";
                case XLThemeColor.Accent3:
                    return "A5A5A5";
                case XLThemeColor.Accent4:
                    return "FFC000";
                case XLThemeColor.Accent5:
                    return "5B9BD5";
                case XLThemeColor.Accent6:
                    return "70AD47";
                case XLThemeColor.Hyperlink:
                    return "0563C1";
                case XLThemeColor.FollowedHyperlink:
                    return "954F72";

                default:
                    return "";
            }
        }

        #region Fill

        public static ShadingPatternValues FromExcelFillPatternValues(XLFillPatternValues xlpattern)
        {
            switch (xlpattern)
            {
                case XLFillPatternValues.Solid:
                    return ShadingPatternValues.Solid;

                case XLFillPatternValues.DarkGray:
                    return ShadingPatternValues.Percent75;

                case XLFillPatternValues.MediumGray:
                    return ShadingPatternValues.Percent50;

                case XLFillPatternValues.LightGray:
                    return ShadingPatternValues.Percent25;

                case XLFillPatternValues.Gray125:
                    return ShadingPatternValues.Percent12;

                case XLFillPatternValues.Gray0625:
                    return ShadingPatternValues.Percent5;

                case XLFillPatternValues.DarkHorizontal:
                    return ShadingPatternValues.HorizontalStripe;

                case XLFillPatternValues.DarkVertical:
                    return ShadingPatternValues.VerticalStripe;

                case XLFillPatternValues.DarkDown:
                    return ShadingPatternValues.ReverseDiagonalStripe;

                case XLFillPatternValues.DarkUp:
                    return ShadingPatternValues.DiagonalStripe;

                case XLFillPatternValues.DarkGrid:
                    return ShadingPatternValues.DiagonalCross;

                case XLFillPatternValues.DarkTrellis:
                    return ShadingPatternValues.Clear;

                case XLFillPatternValues.LightHorizontal:
                    return ShadingPatternValues.ThinHorizontalStripe;

                case XLFillPatternValues.LightDown:
                    return ShadingPatternValues.ThinReverseDiagonalStripe;

                case XLFillPatternValues.LightUp:
                    return ShadingPatternValues.ThinDiagonalStripe;

                case XLFillPatternValues.LightGrid:
                    return ShadingPatternValues.ThinHorizontalCross;

                case XLFillPatternValues.LightTrellis:
                    return ShadingPatternValues.ThinDiagonalCross;

                default:
                    return ShadingPatternValues.Clear;
            }
        }

        #endregion

        #region ColumnWidth



        public static int CalcColWidth(double cellwidth)
        {
            if (Math.Abs(cellwidth) <= 0)
                return 0;

            var factor = 108;
            var intCw = Convert.ToInt32(cellwidth);
            switch (intCw)
            {
                case 0:
                case 1:
                case 2:
                    factor = 134;
                    break;
                case 4:
                case 6:
                case 9:
                case 14:
                    factor = 107;
                    break;
                case 5:
                case 7:
                case 12:
                    factor = 109;
                    break;
                default:
                    factor = 108;
                    break;
            }

            return (int)Math.Truncate(cellwidth * factor);
        }


        #endregion

        public static Color ApplyTint(this Color color, double tint)
        {
            if (tint < -1.0) tint = -1.0;
            if (tint > 1.0) tint = 1.0;

            Color colorRgb = color;
            double fHue = colorRgb.GetHue();
            double fSat = colorRgb.GetSaturation();
            double fLum = colorRgb.GetBrightness();
            if (tint < 0)
            {
                fLum = fLum * (1.0 + tint);
            }
            else
            {
                fLum = fLum * (1.0 - tint) + (1.0 - 1.0 * (1.0 - tint));
            }
            return ToColor(fHue, fSat, fLum);
        }

        internal static Color ToColor(double hue, double saturation, double luminance)
        {
            double chroma = (1.0 - Math.Abs(2.0 * luminance - 1.0)) * saturation;
            double fHue = hue / 60.0;
            double fHueMod2 = fHue;
            while (fHueMod2 >= 2.0) fHueMod2 -= 2.0;
            double fTemp = chroma * (1.0 - Math.Abs(fHueMod2 - 1.0));

            double fRed, fGreen, fBlue;
            if (fHue < 1.0)
            {
                fRed = chroma;
                fGreen = fTemp;
                fBlue = 0;
            }
            else if (fHue < 2.0)
            {
                fRed = fTemp;
                fGreen = chroma;
                fBlue = 0;
            }
            else if (fHue < 3.0)
            {
                fRed = 0;
                fGreen = chroma;
                fBlue = fTemp;
            }
            else if (fHue < 4.0)
            {
                fRed = 0;
                fGreen = fTemp;
                fBlue = chroma;
            }
            else if (fHue < 5.0)
            {
                fRed = fTemp;
                fGreen = 0;
                fBlue = chroma;
            }
            else if (fHue < 6.0)
            {
                fRed = chroma;
                fGreen = 0;
                fBlue = fTemp;
            }
            else
            {
                fRed = 0;
                fGreen = 0;
                fBlue = 0;
            }

            double fMin = luminance - 0.5 * chroma;
            fRed += fMin;
            fGreen += fMin;
            fBlue += fMin;

            fRed *= 255.0;
            fGreen *= 255.0;
            fBlue *= 255.0;

            var red = Convert.ToInt32(Math.Truncate(fRed));
            var green = Convert.ToInt32(Math.Truncate(fGreen));
            var blue = Convert.ToInt32(Math.Truncate(fBlue));

            red = Math.Min(255, Math.Max(red, 0));
            green = Math.Min(255, Math.Max(green, 0));
            blue = Math.Min(255, Math.Max(blue, 0));

            return Color.FromArgb(red, green, blue);
        }
    }


}
