﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Runtime.CompilerServices;
using EV.Public.ReportGraph.Data;
using EV.Public.ReportGraph.Utils;
using EV.Public.ReportGraph.ReplaceExcel;
using EV.Public.ReportGraph.Table;
using Newtonsoft.Json;

namespace EV.Public.ReportGraph.API
{
    public class ReportGraphParameters
    {
        /// <summary>
        /// JSON streng med dokument og Excel filer (Defualt: empty)
        /// </summary>
        public string Inputjsonsstring { get; set; }

        /// <summary>
        /// Temp mappe navn. (Defualt: System/bruger Tempmappe [GetTempPath])
        /// </summary>
        public string Tempfolder { get; set; }

        /// <summary>
        /// Angiver om den stream der returneres er en zip fil med alle debuginfos. (Defualt: false)
        /// </summary>
        public bool Returndebugzip { get; set; }

        /// <summary>
        /// Angiver om den stream der returneres er en PDF i stedet for (word) docx fil. (Default: false)
        /// </summary>
        public bool ReturnPdf { get; set; }

        /// <summary>
        /// Vis debugbeskeder i consolevindue (Defualt: false)
        /// </summary>
        public bool ShowdebugMsg { get; set; }

        /// <summary>
        /// Efter dokomumentet er lavet, skal Tempfolder så slettes (Defualt: true)
        /// </summary>
        public bool Cleanuptempfolder { get; set; }

        /// <summary>
        /// Skal kørslen stoppes ved fejl. (Default: true)
        /// </summary>
        public bool HaltOnErrors { get; set; }

        /// <summary>
        /// Liste med grafer der må bruges i denne kørsel. (Default: "pie,bar,line")
        /// </summary>
        public string AllowedGraphs { get; set; }

        /// <summary>
        /// Skal der generesre en PDF (hvis ReturnPDF er sat, så bliver den automatisk oprettet). (Default: false)
        /// </summary>
        public bool CreatePdf { get; set; }

        public ReportGraphParameters()
        {
            Inputjsonsstring = string.Empty;
            Tempfolder = Path.GetTempPath();
            Returndebugzip = false;
            ShowdebugMsg = false;
            Cleanuptempfolder = true;
            HaltOnErrors = true;
            ReturnPdf = false;
            CreatePdf = false;
            AllowedGraphs = "pie,bar,line";
        }
    }

    public class Handler
    {
        private string _prgVersion = "EV.Public.ReportGraph ver. 11-2019-10-30";

        private Stream _retStream;

        public Stream GenerateReportGraph(ReportGraphParameters reportgraphparameters)
        {
            var logger = new Logger(reportgraphparameters.ShowdebugMsg);
            var mainprgsw = new Stopwatch();
            var replaceexcelHandler = new Handle();
            var replaceTableHandler = new TableHandle();
            
            string doctitle;

            JSONRepObject docitems;

            try
            {
                var basename = $"EV.Public.ReportGraph.{DateTime.Now.Ticks}";
                var tmpdir = reportgraphparameters.Tempfolder;

                // Slet det sidste backslash!
                tmpdir = tmpdir.TrimEnd(Path.DirectorySeparatorChar);

                var tmpstr = Path.Combine(tmpdir + "\\" + basename);
                Directory.CreateDirectory(tmpstr);
                logger.DebugFolder = tmpstr;
                logger.ZipFileName = tmpstr + ".zip";
                mainprgsw.Start();

                logger.Add(_prgVersion);
                logger.Add("Starter process. Parametre er sat som følger:");
                logger.Add($"Tempfolder: {logger.DebugFolder}");
                logger.Add($"Returndebugzip: {reportgraphparameters.Returndebugzip}");
                logger.Add($"ShowdebugMsg: {reportgraphparameters.ShowdebugMsg}");
                logger.Add($"Cleanuptempfolder: {reportgraphparameters.Cleanuptempfolder}");
                logger.Add($"HaltOnErrors: {reportgraphparameters.HaltOnErrors}");
                logger.Add($"ReturnPdf: {reportgraphparameters.ReturnPdf}");
                logger.Add($"AllowGraphs: {reportgraphparameters.AllowedGraphs}");

                File.WriteAllText(logger.DebugFolder + "\\navdata.json", reportgraphparameters.Inputjsonsstring);

                docitems = JsonConvert.DeserializeObject<JSONRepObject>(reportgraphparameters.Inputjsonsstring);

                // Base64 udpakning.
                var sw2 = logger.Start("Pakker base64 objekter ud");
                var iCnt = 1;

                if (string.IsNullOrEmpty(docitems.worddoc))
                    throw new Exception("Word dokumentet mangler / er tomt.");

                docitems.dataStream = new MemoryStream();
                using (var fs = new MemoryStream(Convert.FromBase64String(docitems.worddoc)))
                {
                    fs.CopyTo(docitems.dataStream);
                    docitems.dataStream.Position = 0;
                    docitems.dataStream.Seek(0, SeekOrigin.Begin);
                }

                foreach (var excelelements in docitems.reportgraphs)
                {
                    iCnt++;
                    excelelements.dataStream = !string.IsNullOrEmpty(excelelements.excelfil)
                        ? new MemoryStream(Convert.FromBase64String(excelelements.excelfil))
                        : new MemoryStream();
                    excelelements.dataStream.Position = 0;
                    excelelements.nrused = 0;
                }
                logger.Stop($"Pakkede [{iCnt}] base64 objekter ud", sw2);


                // CleanUp er falsk eller det er til debug zip .. Så gem originalerne på disk..
                if (!reportgraphparameters.Cleanuptempfolder || reportgraphparameters.Returndebugzip)
                {
                    var spt = logger.Start("Gemmer original(e) filer i tempfolder");
                    Helpers.SourcePeak(docitems, logger);
                    logger.Stop($"Gemmer original(e) filer i tempfolder", spt);
                }

                //Replace excel data..
                replaceexcelHandler.ReplaceExcel(docitems, logger, reportgraphparameters.HaltOnErrors, reportgraphparameters.AllowedGraphs);

                // Reaplace table data..
                replaceTableHandler.ReaplaceTables(docitems, logger, reportgraphparameters.HaltOnErrors,reportgraphparameters.AllowedGraphs);
                
                // Set Version.
                DocComment.Set(docitems.dataStream, _prgVersion);

                // Not used index..
                var notusedtems = string.Empty;
                foreach (var excelelements in docitems.reportgraphs)
                {
                    if (excelelements.nrused != 0) continue;

                    if (string.IsNullOrEmpty(notusedtems))
                        notusedtems = excelelements.index;
                    else
                        notusedtems += $",{excelelements.index}";
                }

                if (!string.IsNullOrEmpty(notusedtems))
                    logger.Add($"Følgende excel filer (index) blev ikke brugt: {notusedtems}");

                var wordt = logger.Start("Gemmer det ændrede word doukment");
                doctitle = logger.DebugFolder + "\\worddoc-changed";
                using (Stream savefile = File.OpenWrite(doctitle + ".docx"))
                {
                    docitems.dataStream.Position = 0;
                    docitems.dataStream.CopyTo(savefile);
                    savefile.Close();
                }
                logger.Stop("Gemmer det ændrede word dokument", wordt);

                // Gem som PDF
                if (reportgraphparameters.ReturnPdf || reportgraphparameters.CreatePdf)
                {
                    var pdft = logger.Start("Gemmer som PDF");
                    Helpers.DocToPDF(doctitle + ".docx", doctitle + ".pdf");
                    logger.Stop("Gemmer som PDF", pdft);
                }

                logger.Add("All done - " + logger.DebugFolder);
                logger.SaveToFile(logger.DebugFolder + "\\log.txt", mainprgsw.ElapsedMilliseconds);

            }
            catch (Exception ex)
            {
                if (reportgraphparameters.Cleanuptempfolder)
                {
                    if (Directory.Exists(logger.DebugFolder))
                        Directory.Delete(logger.DebugFolder, true);
                }

                var fejlstr = "GenerateReportGraph fejlede";
                if (!string.IsNullOrEmpty(ex.InnerException?.Message))
                {
                    fejlstr += $": {ex.InnerException.Message}";
                }
                else
                {
                    if (!string.IsNullOrEmpty(ex.Message))
                        fejlstr += $": {ex.Message}";
                }

                if (!string.IsNullOrEmpty(ex.StackTrace))
                {
                    fejlstr += $"[Stack Trace]: {ex.StackTrace}";
                }

                if (!reportgraphparameters.Cleanuptempfolder)
                {
                    logger.Add(fejlstr);
                    logger.SaveToFile(logger.DebugFolder + "\\log.txt", mainprgsw.ElapsedMilliseconds);
                }

                throw new Exception(fejlstr);
            }

            _retStream = new MemoryStream();
            if (reportgraphparameters.Returndebugzip)
            {
                ZipFile.CreateFromDirectory(logger.DebugFolder, logger.ZipFileName);
                var fs = File.OpenRead(logger.ZipFileName);
                fs.CopyTo(_retStream);
                fs.Close();
            }
            else
            {
                if (reportgraphparameters.ReturnPdf)
                {
                    var fs = File.OpenRead(doctitle + ".pdf");
                    fs.CopyTo(_retStream);
                    fs.Close();
                }
                else
                {
                    docitems.dataStream.Position = 0;
                    docitems.dataStream.CopyTo(_retStream);
                    docitems.dataStream.Close();
                }
            }

            // DeleteTempFolder
            if (reportgraphparameters.Cleanuptempfolder)
            {
                if (Directory.Exists(logger.DebugFolder))
                    Directory.Delete(logger.DebugFolder, true);
            }

            _retStream.Position = 0;
            return _retStream;
        }
    }
}
