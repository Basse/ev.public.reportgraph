﻿using EV.Public.ReportGraph.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using EV.Public.ReportGraph.Utils;
using Cell = DocumentFormat.OpenXml.Spreadsheet.Cell;
using DataTable = System.Data.DataTable;
using Row = DocumentFormat.OpenXml.Spreadsheet.Row;
using System.Globalization;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

// ReSharper disable UnusedMember.Local

namespace EV.Public.ReportGraph.ReplaceExcel
{
    public class Handle
    {
        private Logger _logger;

        private double ToDouble(string value)
        {
            try
            {
                return double.Parse(Regex.Replace(value, "[.,]", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));
            }
            catch
            {
                _logger?.AddError($"Convert to double: failed with value [{value}] - returned 0.0");
                return 0.0;
            }
        }

        private ChartData ExcelToCharData(Stream excelstream, bool transpose)
        {
            var dataTable = new DataTable();
            using (var spreadSheetDocument = SpreadsheetDocument.Open(excelstream, false))
            {
                var sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                var relationshipId = sheets.First().Id.Value;
                var worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                var workSheet = worksheetPart.Worksheet;
                var sheetData = workSheet.GetFirstChild<SheetData>();
                var rows = sheetData.Descendants<Row>();

                var enumRows = rows as Row[] ?? rows.ToArray();
                foreach (var openXmlElement in enumRows.ElementAt(0))
                {
                    var cell = (Cell)openXmlElement;
                    if (!string.IsNullOrEmpty(cell.InnerText))
                        dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                }

                foreach (var row in enumRows)
                {
                    if (string.IsNullOrEmpty(row.InnerText)) continue;
                    var dataRow = dataTable.NewRow();

                    for (var i = 0; i < row.Descendants<Cell>().Count(); i++)
                        dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));

                    dataTable.Rows.Add(dataRow);
                }

            }

            var chartData = new ChartData
            {
                SeriesNames = new string[] { },
                CategoryDataType = ChartDataType.String,
                CategoryNames = new string[] { },
                Values = new double[][] { }
            };


            if (dataTable.Rows.Count <= 1) return chartData;

            var lSeriesNames = new List<string>();
            var lCategoryNames = new List<string>();

            var firstrow = true;
            for (var rCnt = 0; rCnt < dataTable.Rows.Count; rCnt++)
            {
                var rItems = dataTable.Rows[rCnt].ItemArray;

                // First Row. 
                if (firstrow)
                {
                    for (var i = 1; i < rItems.Length; i++)
                        lSeriesNames.Add(rItems[i].ToString());

                    chartData.SeriesNames = lSeriesNames.ToArray();
                    firstrow = false;
                }
                else
                    lCategoryNames.AddRange(rItems.Where((t, rInt) => rInt == 0).Select(t => t.ToString()));

                chartData.CategoryNames = lCategoryNames.ToArray();
            }


            // Get data columnwise
            var lValues = new List<double[]>();
            for (var lSerCnt = 0; lSerCnt < lSeriesNames.Count; lSerCnt++)
            {
                var singleRow = new List<double>();
                for (var rCnt = 1; rCnt < dataTable.Rows.Count; rCnt++)
                {
                    var cnv = dataTable.Rows[rCnt].ItemArray[lSerCnt + 1].ToString();
                    singleRow.Add(string.IsNullOrEmpty(cnv) ? 0 : ToDouble(cnv));
                }
                lValues.Add(singleRow.ToArray());
            }
            chartData.Values = lValues.ToArray();

            if (!transpose) return chartData;

            // Transpose data.
            var chartDataTransposed = new ChartData
            {
                SeriesNames = new string[] {  },
                CategoryDataType = ChartDataType.String,
                CategoryNames = new string[] { },
                Values = new double[][] { }
            };

            chartDataTransposed.SeriesNames = chartData.CategoryNames;
            chartDataTransposed.CategoryNames = chartData.SeriesNames;

            var rValues = new List<double[]>();

            for (var x = 0; x <= chartData.Values[0].GetUpperBound(0); x++)
            {
                var singlerow = new List<double>();
                for (var y = 0; y <= chartData.Values.GetUpperBound(0); y++)
                {
                    singlerow.Add(chartData.Values[y][x]);
                }

                rValues.Add(singlerow.ToArray());
            }
            chartDataTransposed.Values = rValues.ToArray();

            /*
            for (var lSerCnt = 1; lSerCnt <= chartDataTransposed.SeriesNames.Length; lSerCnt++)
            {
                var singlecols = new List<double>();
                for (var rCnt = 1; rCnt < dataTable.Columns.Count; rCnt++)
                {
                    var cnv = dataTable.Rows[lSerCnt].ItemArray[rCnt].ToString();
                    singlecols.Add(string.IsNullOrEmpty(cnv) ? 0 : ToDouble(cnv));
                }
                rValues.Add(singlecols.ToArray());
            }
            chartDataTransposed.Values = rValues.ToArray();
            */

            return chartDataTransposed;
        }

        /*
                private static string ToExcelInteger(DateTime dateTime)
                {
                    return dateTime.ToOADate().ToString();
                }
        */
        private enum Formats
        {
            General = 0,
            Number = 1,
            Decimal = 2,
            Currency = 164,
            Accounting = 44,
            DateShort = 14,
            DateLong = 165,
            Time = 166,
            Percentage = 10,
            Fraction = 12,
            Scientific = 11,
            Text = 49
        }
        private static string GetCellValue(SpreadsheetDocument document, CellType cell)
        {
            try
            {
                var stringTablePart = document.WorkbookPart.SharedStringTablePart;
                if (cell.CellValue == null)
                    return "";
                var value = cell.CellValue.InnerXml;

                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                    return stringTablePart.SharedStringTable.ChildElements[int.Parse(value)].InnerText;

                if (cell.StyleIndex == null)
                    return value;

                var styleIndex = (int)cell.StyleIndex.Value;
                var cellFormat = (CellFormat)document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex);
                var formatId = cellFormat.NumberFormatId.Value;

                if (formatId == (uint)Formats.DateShort || formatId == (uint)Formats.DateLong)
                {
                    if (double.TryParse(cell.InnerText, out var oaDate))
                    {
                        value = DateTime.FromOADate(oaDate).ToShortDateString();
                    }
                }
                else
                    value = cell.InnerText;

                return value;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static Reportgraph FindItemByIndex(IEnumerable<Reportgraph> srcList, int srcIndex)
        {
            foreach (var rItem in srcList)
            {
                if (rItem.index != srcIndex.ToString()) continue;
                rItem.nrused++;
                return rItem;
                //return rItem.dataStream;                                      
            }
            return null;
        }


        public void ReplaceExcel(JSONRepObject repobj, Logger ilogger, bool stoponerror, string allowedgraphs)
        {
            _logger = ilogger;
            var reTimer = _logger.Start("Starter erstatning af excelregneark");
            using (var doc = WordprocessingDocument.Open(repobj.dataStream, true))
            {
                var drawingIdx = 1;

                var sdtElementDrawing = doc.MainDocumentPart.Document.Descendants<DocumentFormat.OpenXml.Wordprocessing.Drawing>().ToList();
                _logger.Add($"Fandt {sdtElementDrawing.Count} diagrammer i word dokumentet");

                foreach (var d in sdtElementDrawing)
                {

                    var gtimer = _logger.Start($"Arbejder på diagram #{drawingIdx}");

                    var cr = d?.Descendants<ChartReference>().FirstOrDefault();
                    if (cr == null)
                    {
                        var txt = $"Advarsel - diagram #{drawingIdx}: CharReference blev ikke fundet. (Ej et gyldigt Chart - index forbliver uændret).";
                        _logger.Add(txt);
                        if (stoponerror)
                            throw new Exception(txt);
                        continue;
                    }

                    var mdpId = doc.MainDocumentPart.Parts.FirstOrDefault(pt => pt.RelationshipId == cr?.Id);
                    var cp = (ChartPart)mdpId?.OpenXmlPart;
                    if (cp == null)
                    {
                        var txt = $"Advarsel - diagram #{drawingIdx}: ChartPart blev ikke fundet.";
                        _logger.Add(txt);
                        if (stoponerror)
                            throw new Exception(txt);
                    }
                    else
                        _logger.Add($"Diagram er af typen [{SupportedGraphs.IsTypeOf(cp)}]");

                    if (SupportedGraphs.IsValid(cp, allowedgraphs))
                    {

                        var rgItem = FindItemByIndex(repobj.reportgraphs, drawingIdx);
                        if (rgItem == null)
                        {
                            var txt = $"der blev ikke fundet et reportGraph item til diagram #{drawingIdx}";
                            _logger.Add(txt);
                            if (stoponerror)
                                throw new Exception(txt);

                        }

                        // Set lineoptions.
                        if (rgItem != null && rgItem.lineOptions == null)
                        {
                           rgItem.lineOptions = new LineOptions();
                           if (! string.IsNullOrEmpty(rgItem.options))
                               rgItem.lineOptions = JsonConvert.DeserializeObject<LineOptions>("{" + rgItem.options + "}");                            
                        }


                        var excelstream = rgItem?.dataStream;
                        if (excelstream == null)
                        {
                            var txt = $"der blev ikke fundet noget regneark til diagram #{drawingIdx}";
                            _logger.Add(txt);
                            if (stoponerror)
                                throw new Exception(txt);
                        }
                        else
                        {
                            _logger.Add(string.IsNullOrEmpty(rgItem.options)
                                ? "Options: [ingen]"
                                : $"Options: [{rgItem.options}]");


                            var chData = ExcelToCharData(excelstream, rgItem.lineOptions.TransposeDiagram);
                            if (chData.SeriesNames.Any())
                            {
                                EVChartUpdater.UpdateChart(cp, chData);

                                var title = string.Empty;
                                var _logtxt = "Title (not set in options or excel)";
                                if (string.IsNullOrEmpty(rgItem.lineOptions.CaptionDiagram))
                                {
                                    title = GraphTitle.GetExcelTitle(excelstream);
                                    _logtxt = $"Title (excel a1): [{title}]";
                                }
                                else
                                {
                                    title = rgItem.lineOptions.CaptionDiagram;
                                    _logtxt = $"Title (option): [{title}]";
                                }

                                if (!string.IsNullOrEmpty(title))
                                    GraphTitle.UpdateTitle(cp, title);

                                _logger.Add(_logtxt);
                            }
                            else
                            {
                                var txt = $"Excel arket til diagram #{drawingIdx} er tomt!";
                                _logger.Add(txt);
                                if (stoponerror)
                                    throw new Exception(txt);
                            }
                        }

                    }
                    else
                    {
                        var txt = $"Advarsel - diagram #{drawingIdx}: Diagramtypen [{SupportedGraphs.IsTypeOf(cp)}] er ikke implementeret";
                        _logger.Add(txt);
                        if (stoponerror)
                            throw new Exception(txt);
                    }

                    _logger.Stop($"Arbejder på diagram #{drawingIdx}", gtimer);
                    drawingIdx++;
                }
            }
            _logger.Stop("Starter erstatning af excelregneark", reTimer);
        }
    }
}

