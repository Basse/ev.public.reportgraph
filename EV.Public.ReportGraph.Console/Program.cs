﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EV.Public.ReportGraph.Data;
using EV.Public.ReportGraph.Utils;


namespace EV.Public.ReportGraph.Console
{
    class Program
    {
        public class testfiledef
        {
            public string filename { get; set; }
            public string options { get; set; }
            public string index { get; set; }
        }

        static void Main(string[] args)
        {
            try
            {
                // API Text..
                var myApi = new API.Handler();
                var gendocparm = new API.ReportGraphParameters();

                var fileList = new List<testfiledef>()
                {
                    /*
                    //Overskrifter bliver ikke sat ved re-use af doc.

                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\worddoc_reuse.docx",
                        index = ""
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\JHJ_reuse.xlsx",
                        index = "1"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\JHJ_reuse.xlsx",
                        index = "2"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\JHJ_reuse.xlsx",
                        index = "Tab1"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\JHJ_reuse.xlsx",
                        index = "Tab2"
                    }

                    /*
                    //Overskrifter bliver ikke sat ved re-use af doc.
                    
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\tilpeter_nocChange.docx",
                        index = ""
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\JHJ.xlsx",
                        index = "1"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\JHJ.xlsx",
                        index = "2"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\JHJ.xlsx",
                        index = "Tab1"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\OverskfiterReuse\JHJ.xlsx",
                        index = "Tab2"
                    }
                                         
                    // Overskrifter bliver ikke sat ved re-yse af doc (slut).
                    /*
                    
                    //Pie med manglende pPr.
                    /*
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\Diagram-Pie-3.docx",
                        index = ""
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-pie-s1-c4.xlsx",
                        index = "1"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-pie-s1-c4.xlsx",
                        index = "2"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-pie-s1-c4.xlsx",
                        index = "3"
                    }                    
                    // Pie med manglende pPr.
                    */

                    // Surface med overskrift.
                    /* Start surface
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\Diagram-Surface-4.docx",
                        index = ""
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                        index = "1"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                        index = "2"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                        index = "3"
                    },
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                        index = "4"
                    }
                    */

                    // første filnavn er word doc.
                    // Column-Line-Pie
                    // @"..\..\..\Testdata\AllTypes\Diagram-Column-Line-Pie.docx",

                    // Bar-Area-Surface-Radar
                    //@"..\..\..\Testdata\AllTypes\Diagram-Bar-Area-Surface-Radar.docx",

                    // Column-Bar
                    //new testfiledef()
                    //{
                    //    filename = @"..\..\..\Testdata\AllTypes\Diagram-Column-Bar.docx",
                    //    index = ""
                    //},

                    // Column-Table-Bar-Table
                    //new testfiledef()
                    //{
                    //    filename = @"..\..\..\Testdata\AllTypes\Diagram-Column-Table-Bar-Table.docx",
                    //    index = ""
                    //},

                    // Test table error (placement).
                    /*
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\Overskrift\skabelondoc.docx",
                        index = ""
                    },                           
                    // Table excel filer..
                    new testfiledef()
                    {
                        filename =  @"..\..\..\Testdata\Overskrift\1.xlsx",
                        index = "1"
                    },
                    new testfiledef()
                    {
                        filename =  @"..\..\..\Testdata\Overskrift\2.xlsx",
                        index = "2"
                    },
                    new testfiledef()
                    {
                        filename =  @"..\..\..\Testdata\Overskrift\tab1.xlsx",
                        index = "tab1"
                    },
                    new testfiledef()
                    {
                        filename =  @"..\..\..\Testdata\Overskrift\tab2.xlsx",
                        index = "tab2"
                    }
                    // Test Tabel placement slut.
                    */


                    // Fill patterns
                    //new testfiledef()
                    //{
                    //    filename =  @"..\..\..\Testdata\Tables\Fill-Patterns.xlsx",
                    //    index = "Alt Table 1"
                    //}

                    // Colors in cell
                    //new testfiledef()
                    //{
                    //filename = @"..\..\..\Testdata\Background Color\Plain Colors.xlsx",
                    //    index = "Alt Table 1"
                    //}

                    // Plain Borders..
                    //new testfiledef()
                    //{
                    //    filename = @"..\..\..\Testdata\Borders\Plain Borders.xlsx",
                    //    index = "Alt Table 1"
                    //}

                    //new testfiledef()
                    //{
                    //    filename = @"..\..\..\Testdata\AllTypes\excel-ark-1-jhj.xlsx",
                    //    index = "Alt Table 1"

                    //}, 
                    //new testfiledef()
                    //{
                    //    filename = @"..\..\..\Testdata\AllTypes\excel-ark-2-jhj.xlsx",
                    //    index = "Alt Table 2"

                    //}
                    //,

                    // Column-Bar
                    //new testfiledef()
                    //{
                    //    filename =  @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //    index = "1"

                    //}, 
                    //new testfiledef()
                    //{
                    //    filename = @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //    index = "2"

                    //}, 
                    //new testfiledef()
                    //{
                    //    filename = @"..\..\..\Testdata\AllTypes\excel-ark-1-jhj.xlsx",
                    //    index = "Alt Table 1"

                    //}, 
                    //new testfiledef()
                    //{
                    //    filename = @"..\..\..\Testdata\AllTypes\excel-ark-2-jhj.xlsx",
                    //    index = "Alt Table 2"

                    //}



                    //// Bar-Area-Surface-Radar
                    //// Bar x 6
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",

                    //// Area x 6
                    //@"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",

                    //// Surface x 4
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",

                    //// Radar x 3
                    //@"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx"


                    //// Column-Line-Pie
                    //// Column x 7
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //// Line x 7
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                    //// Pie x 3
                    //@"..\..\..\Testdata\AllTypes\excel-pie-s1-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-pie-s1-c4.xlsx",
                    //@"..\..\..\Testdata\AllTypes\excel-pie-s1-c4.xlsx"

                    // Pictures (test)
                    new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\Bugs\Pictures\Sk_med_billede.docx",
                        index = ""
                    },

                    // Tabel 1 (data).
                    new testfiledef()
                    {
                        filename =  @"..\..\..\Testdata\Bugs\Pictures\data1.xlsx",
                        options = "TransposeDiagram: true, CaptionDiagram: 'Dette er en test'",
                        index = "1"
                    }, 

                    // Tabel 2 (data)
                    new testfiledef()
                    {
                        filename =  @"..\..\..\Testdata\Bugs\Pictures\data2.xlsx",
                        options = "TransposeDiagram: false",
                        index = "2"
                    }
                };

                // All Types.. all diagrams.
                /*
                fileList.Add(new testfiledef()
                {
                    filename = @"..\..\..\Testdata\AllTypes\AllTypes_34_Diagrams.docx",
                    index = ""
                });

                // Pie 3
                int cnt;
                int idx = 1;
                for (cnt = 0; cnt < 3; cnt++)
                {
                    System.Console.WriteLine($"Pie (3) Idx: {idx}");

                    fileList.Add(new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-pie-s1-c4.xlsx",
                        index = idx++.ToString()
                    });
                }

                // bar 6
                for (cnt = 0; cnt < 6; cnt++)
                {
                    System.Console.WriteLine($"Bar (6) Idx: {idx}");

                    fileList.Add(new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                        index = idx++.ToString()
                    });

                }

                // Column 7
                for (cnt = 0; cnt < 7; cnt++)
                {
                    System.Console.WriteLine($"Column (7) Idx: {idx}");

                    fileList.Add(new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                        index = idx++.ToString()
                    });

                }

                
                // Line 7
                for (cnt = 0; cnt < 7; cnt++)
                {
                    System.Console.WriteLine($"Line (7) Idx: {idx}");

                    fileList.Add(new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                        index = idx++.ToString()
                    });

                }

                
                // Area 6
                for (cnt = 0; cnt < 6; cnt++)
                {
                    System.Console.WriteLine($"Area (6) Idx: {idx}");

                    fileList.Add(new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",
                        index = idx++.ToString()
                    });

                }

                // Surface 4
                for (cnt = 0; cnt < 4; cnt++)
                {
                    System.Console.WriteLine($"Surface (4) Idx: {idx}");

                    fileList.Add(new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s3-c4.xlsx",
                        index = idx++.ToString()
                    });

                }


                // Radar 3
                for (cnt = 0; cnt < 3; cnt++)
                {
                    System.Console.WriteLine($"Radar (3) Idx: {idx}");

                    fileList.Add(new testfiledef()
                    {
                        filename = @"..\..\..\Testdata\AllTypes\excel-s2-d6.xlsx",
                        index = idx++.ToString()
                    });

                }
                */
                // All Types test
                

                if (fileList.Any())
                {

                    var tempJSonObj = new JSONRepObject
                    {
                        reportgraphs = new List<Reportgraph>()
                    };

                    var msdoc = true;
                    foreach (var filerec in fileList)
                    {
                        using (Stream docf = File.OpenRead(filerec.filename))
                        {
                            var tempStream = new MemoryStream();
                            docf.CopyTo(tempStream);
                            tempStream.Position = 0;
                            tempStream.Seek(0, SeekOrigin.Begin);

                            if (msdoc)
                            {
                                tempJSonObj.worddoc = Convert.ToBase64String(tempStream.ToArray());                                
                                msdoc = false;
                            }
                            else
                            {
                                tempJSonObj.reportgraphs.Add(new Reportgraph()
                                {
                                    excelfil = Convert.ToBase64String(tempStream.ToArray()),
                                    options =  filerec.options,
                                    index = filerec.index,
                                });
                            }
                        }
                    }
                    gendocparm.Inputjsonsstring = Newtonsoft.Json.JsonConvert.SerializeObject(tempJSonObj);
                }
                else
                {
                    using (var r = new StreamReader(@"..\..\..\Testdata\Bugs\navdata_2.json")) // @"c:\temp\Regulering_05-01-18.json"))
                    {
                        gendocparm.Inputjsonsstring = r.ReadToEnd();
                    }
                }

                // Tempmappe
                gendocparm.Tempfolder = @"C:\temp\";
                // dont Clean up..
                gendocparm.Cleanuptempfolder = false;

                // Show Messages in Consolewindow
                gendocparm.ShowdebugMsg = true;

                // Vi tillader at køre selvom der opstår fejl.
                gendocparm.HaltOnErrors = false;

                // Lav PDF
                gendocparm.CreatePdf = false;

                // pie,bar,column,line,area,surface,radar
                gendocparm.AllowedGraphs = "pie,bar,column,line,area,surface,radar";

                myApi.GenerateReportGraph(gendocparm);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"fejlede med: {ex.Message}");
            }

            System.Console.WriteLine("Done..");
            System.Console.Read();

        }

    }
}
